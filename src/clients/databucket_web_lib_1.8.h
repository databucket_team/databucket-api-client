/*************************************************************************
The package of methods to deal with Databucket REST Service
Works with Databucket REST Service version: 1.8
Library version: 1.1
Last update: 2017-10-12
Author: Krzyszot S�ysz
Visit: www.databucket.pl
**************************************************************************/


/*
	Enumeration type definition for boolean values
*/
typedef enum {false = 0, true = 1, null = -1} bool;

/*
Description:
	Saves the table list of tags as dynamic parameters
	For every tag is created parameter with the name equals tag name and value eqals tag id.
Parameters:
	tableName - name of the source table
Example:
	//Table tags: [{"tagId":21,"tagName":"potential"},{"tagId":22,"tagName":"new"}]
	
	char * sampleTable = "clients"; // set the table name exists in your Databucket database
	
	// Save the table list of tags as dynamic parameters
	databucketGetTags(sampleTable);
	
	// Get bundles with given tags ids
	databucketGetBundles(sampleTable, "", lr_eval_string("{tags.new}"), "");
*/
void databucketGetTags(char *tableName) {
	int i, tagsCount;
	char *queryString = (char *) malloc(1 + strlen("QueryString=$.tags[i].tagId") + 3);
	
	char *url = (char *) malloc(1 + strlen("URL={databucketURL}/tables/%s/tags") + strlen(tableName));
	sprintf(url, "URL={databucketURL}/tables/%s/tags", tableName);
	web_reg_find("Text=\"status\":\"OK\"", LAST);
    web_reg_save_param_regexp ("ParamName=responseBody", "RegExp=(.*)", SEARCH_FILTERS, "Scope=Body", LAST);
    web_custom_request("GetTags", 
        url,
        "Method=GET", 
        "EncType=application/json", 
        LAST);   
    lr_convert_string_encoding(lr_eval_string("{responseBody}"), LR_ENC_UTF8, LR_ENC_SYSTEM_LOCALE, "responseBody");
    lr_eval_json("Buffer={responseBody}", "JsonObject=databucket", LAST);
    tagsCount = lr_json_get_values("JsonObject=databucket", "ValueParam=tagId", "QueryString=$..tagId", "SelectAll=Yes", "NotFound=Continue", LAST);
    for (i = 0; i < tagsCount; i++) {
    	sprintf(queryString, "QueryString=$.tags[%d].tagName", i);
    	lr_json_get_values("JsonObject=databucket", "ValueParam=tagName", queryString, LAST);
    	sprintf(queryString, "QueryString=$.tags[%d].tagId", i);
    	lr_json_get_values("JsonObject=databucket", lr_eval_string("ValueParam=tags.{tagName}"), queryString, LAST);
    }
}

/*
Description:
	Inserts new bundle into the table
Parameters:
	tableName - name of the source table
	tagId - the bundle tag id
	description - the bundle description
	lock - set true to lock the bundle, set false to unlock the budnle
	properties - json string contains the budnle properties. Retrieve this using getProperties() method.

Example:
	char * sampleTable = "tablename"; // set the table name exists in your Databucket database
	int sampleTag = 19;	// set the tag id exists in the selected table
	char * sampleDescription = ""; // you can type description message
	
	// Create the json bundle object
	bundleCreate();
	
	// Set the bundle properties
	bundleSetProperty("param1", "value1");
	bundleSetProperty("param2", "value2");
	bundleSetProperty("param3", "value3");
	
	// Save all properties as dynamic parameter with the 'properties' name
	bundleGetProperties();
	
	// Insert the bundle into database and update the json bundle object
	databucketInsertBundle(sampleTable, sampleTag, sampleDescription, false, lr_eval_string("{properties}"));
*/
void databucketInsertBundle(char* tableName, int tagId, char* description, bool lock, char* properties) {
	char *url = (char *) malloc(1 + strlen("URL={databucketURL}/tables/{tableName}/bundles?userName={GN}.{VU_ID}&tagId=&description=&lock=false") + strlen(tableName) + strlen("&tagId=") + strlen(convertPlainToURL(description)));
    char *body = (char *) malloc(1 + strlen("Body=") + strlen(properties));
    
    char *lockStr = "false";
    if (lock == true) lockStr = "true";
    
    sprintf(url, "URL={databucketURL}/tables/%s/bundles?userName={GN}.{VU_ID}&tagId=%d&description=%s&lock=%s", tableName, tagId, convertPlainToURL(description), lockStr);
	sprintf(body, "Body=%s", properties);
    lr_json_get_values("JsonObject=bundle", "ValueParam=properties", "QueryString=$.properties", LAST);
    lr_convert_string_encoding(lr_eval_string("{properties}"), LR_ENC_SYSTEM_LOCALE, LR_ENC_UTF8, "properties");
    web_reg_find("Text=\"status\":\"OK\"", LAST);
    web_reg_save_param_regexp ("ParamName=responseBody", "RegExp=(.*)", SEARCH_FILTERS, "Scope=Body", LAST);
    web_custom_request("databucketInsertBundle",
        url, 
        "Method=POST", 
        "EncType=application/json", 
        body,
        LAST); 
    lr_convert_string_encoding(lr_eval_string("{responseBody}"), LR_ENC_UTF8, LR_ENC_SYSTEM_LOCALE, "responseBody");
    lr_eval_json("Buffer={responseBody}", "JsonObject=databucket", LAST);
    bundlesGetBundle(0);
}


/*
Description:
	Pernamently deletes the bundles specified by it table name and ids
Parameters:
	tableName - the table name
	bundlesIds - the list of bundles ids to delete
Example:
	char * sampleTable = "tablename"; // set the table name exists in your Databucket database
	char * bundlesIds = "4,5"; // set the list of bundles ids
	
	databucketDeleteBundles(sampleTable, bundlesIds);
*/
void databucketDeleteBundles(char* tableName, char* bundlesIds) {	
	char *url = (char *) malloc(1 + strlen("URL={databucketURL}/tables/{tableName}/bundles?userName={GN}.{VU_ID}&bundlesIds=") + strlen(tableName) + strlen(lr_eval_string("{GN}.{VU_ID}")) + strlen(bundlesIds));
	sprintf(url, "URL={databucketURL}/tables/%s/bundles?userName={GN}.{VU_ID}&bundlesIds=%s", tableName, bundlesIds);
    web_reg_find("Text=\"status\":\"OK\"", LAST);
	web_custom_request("databucketDeleteBundles",
        url, 
        "Method=DELETE", 
        "EncType=application/json", 
        LAST); 
}

/*
Description:
	Gives the list of bundles specified by its ids
	As the result, it creates 'bundle' json object for first bundle 
Parameters:
	tableName - the source table name
	bundlesIds - the list of bundles (can be empty string)
	tagsIds - the list of tags (can be empty string)
	properties - puts into sql select query ...where properties like ('<properties>')
Example:
	char * sampleTable = "tablename"; // set the table name exists in your Databucket database
	char * sampleBundlesIds = "8,15,12";
	char * sampleTagsIds = "19,20";
	char * sampleProperties = "%\"name\":\"John\"%";
	
	// Get all bundles from the given table
	databucketGetBundles(sampleTable, "", "", "");
	
	// Get bundles with given bundles ids
	databucketGetBundles(sampleTable, sampleBundlesIds, "", "");
	
	// Get bundles with given tags ids
	databucketGetBundles(sampleTable, "", sampleTagsIds, "");
	
	// Get bundles for which properties are like <properties> parameter
	databucketGetBundles(sampleTable, "", "", sampleProperties);
	
	// Get bundles with given tags ids for which properties are like <properties> parameter
	databucketGetBundles(sampleTable, "", sampleTagsIds, sampleProperties);
*/
void databucketGetBundles(char* tableName, char* bundlesIds, char* tagsIds, char* properties) {
	char* url = (char *) malloc(1 + strlen("URL={databucketURL}tables/{tableName}/bundles?bundlesIds=&tagsIds&properties&") + strlen(tableName) + strlen(bundlesIds) + strlen(tagsIds) + strlen(convertPlainToURL(properties)));
	
	//lr_log_message("bundlesIds len = %d", strlen(bundlesIds));
	if (bundlesIds != "") {
		sprintf(url, "URL={databucketURL}/tables/%s/bundles?bundlesIds=%s", tableName, bundlesIds);
	} else if (tagsIds != "" && properties != "") {
		sprintf(url, "URL={databucketURL}/tables/%s/bundles?tagsIds=%s&properties=%s", tableName, tagsIds, convertPlainToURL(properties));
	} else if (tagsIds != "") {
		sprintf(url, "URL={databucketURL}/tables/%s/bundles?tagsIds=%s", tableName, tagsIds);
	} else if (properties != "") {
		sprintf(url, "URL={databucketURL}/tables/%s/bundles?properties=%s", tableName, convertPlainToURL(properties));
	} else {
		sprintf(url, "URL={databucketURL}/tables/%s/bundles", tableName);
	}
	
	web_reg_find("Text=\"status\":\"OK\"", LAST);
    web_reg_save_param_regexp ("ParamName=responseBody", "RegExp=(.*)", SEARCH_FILTERS, "Scope=Body", LAST);
    web_custom_request("databucketGetBundles", 
        url,
        "Method=GET", 
        "EncType=application/json", 
        LAST);       
    lr_convert_string_encoding(lr_eval_string("{responseBody}"), LR_ENC_UTF8, LR_ENC_SYSTEM_LOCALE, "responseBody");
    lr_eval_json("Buffer={responseBody}", "JsonObject=databucket", LAST);
    bundlesGetBundle(0);
}


/*
Description:
	Locks and returns as 'bundles' jedon object, a list of bundles with gieven tags ids
Parameters:
	tableName - the source table name
	tagsIds - the list of tags ids
	random - true/false (random/sequential)
	count - number of bundles to lock
Example:
	char * sampleTable = "tablename"; // set the table name exists in your Databucket database
	char * sampleTagsIds = "19";
	
	// Lock and get 1 bundle with tag id 19
	databucketLockBundles(sampleTable, sampleTagsIds, false, 1);
	
	// Lock and get 3 bundles with tag id 19
	databucketLockBundles(sampleTable, sampleTagsIds, false, 3);
	
	// Lock and get 1 random bundle with tag id 19 or 20
	databucketLockBundles(sampleTable, "19,20", true, 1);
	
	// Lock and get 5 random bundles with tag id 19
	databucketLockBundles(sampleTable, sampleTagsIds, true, 5);
*/
void databucketLockBundles(char* tableName, char* tagsIds, bool random, int count) {
	char* url = (char *) malloc(1 + strlen("URL={databucketURL}/tables/{tableName}/bundles/lock?userName={GN}.{VU_ID}tagsIds=&random=&count=") + strlen(tableName) + strlen(tagsIds) + strlen("false") + 5);
	
	char *randomStr = "false";
    if (random == true)	randomStr = "true";
    
	sprintf(url, "URL={databucketURL}/tables/%s/bundles/lock?userName={GN}.{VU_ID}&tagsIds=%s&random=%s&count=%d", tableName, tagsIds, randomStr, count);
	
	web_reg_find("Text=\"status\":\"OK\"", LAST);
    web_reg_save_param_regexp ("ParamName=responseBody", "RegExp=(.*)", SEARCH_FILTERS, "Scope=Body", LAST);
    web_custom_request("databucketLockBundles", 
        url,
        "Method=GET", 
        "EncType=application/json", 
        LAST);   
    lr_convert_string_encoding(lr_eval_string("{responseBody}"), LR_ENC_UTF8, LR_ENC_SYSTEM_LOCALE, "responseBody");
    lr_eval_json("Buffer={responseBody}", "JsonObject=databucket", LAST);
    bundlesGetBundle(0);
}


/*
Description:
	Updates the bundle by given function attributes
Parameters:
	tableName - the table name
	bundleId - the bundle id
	newTagId - new tag id for given bundle
	newDescription - new description for given bundle
	newLock - new lock (true/false) for given bundle
	newProperties - new properties (json structure) for given bundle
Example:
	char * sampleTable = "tablename"; // set the table name exists in your Databucket database
	char * sourceTagsIds = "19"; // source tag id
	int destTagId = 20; // destination tag id
	
	// Lock and get 1 bundle with tag id 19
	databucketLockBundles(sampleTable, sourceTagsIds, false, 1);
	
	// Save the bundle id into dynamic parameter
	bundleGetBundleId();
	
	// Save property 'param1' as dynamic parameter
	bundleGetProperty("param1");
	
	// Now you can use property 'param1'
	lr_log_message(lr_eval_string("Value for param1: {param1}"));
	
	// Add new proprty to the bundle
	bundleSetProperty("newProperty", "newPropertyValue");
	
	// Remove property 'param2'
	bundleDeleteProperty("param2");
	
	// Save all properties as dynamic parameter 'properties'
	bundleGetProperties();
	
	// Only unlock the bundle
	databucketUpdateBundle(sampleTable, atoi(lr_eval_string("bundleId")), -1, "", false, "");
	
	// Change only the bundle description
	databucketUpdateBundle(sampleTable, atoi(lr_eval_string("bundleId")), -1, "sample description", null, "");
	
	// Change only the bundle tag
	databucketUpdateBundle(sampleTable, atoi(lr_eval_string("bundleId")), destTagId, "", null, "");
	
	// Change only the bundle properties
	databucketUpdateBundle(sampleTable, atoi(lr_eval_string("bundleId")), -1, "", null, lr_eval_string("{properties}"));
	
	// Change all fields
	databucketUpdateBundle(sampleTable, atoi(lr_eval_string("bundleId")), destTagId, "another description", false, lr_eval_string("{properties}"));
*/
void databucketUpdateBundle(char* tableName, int bundleId, int newTagId, char* newDescription, bool newLock, char* newProperties) {
	char* url = (char *) malloc(1 + strlen("URL={databucketURL}/tables/{tableName}/bundles?userName=VU&bundleId=&newTagId=&newDescription=&newLock=false") + strlen(tableName) + strlen(convertPlainToURL(newDescription)) + strlen(newProperties) + 15);
	char* body = (char *) malloc(1 + strlen("Body=") + strlen(newProperties));
	char* sBundleId = (char *) malloc(1 + strlen("&bundleId=") + 5);
	char* sTagId = (char *) malloc(1 + strlen("&newTagId=") + 5);
	char* sDescription = (char *) malloc(1 + strlen("&newDescription=") + strlen(convertPlainToURL(newDescription)));
	char* sLock = (char *) malloc(1 + strlen("&newLock=false"));
	
    sprintf(url, "URL={databucketURL}/tables/%s/bundles?userName={GN}.{VU_ID}&bundleId=%d", tableName, bundleId);
	
	if (bundleId > 0) {
		sprintf(sBundleId, "&bundleId=%d", bundleId);
		strcat(url, sBundleId);
	}
	
	if (newTagId > 0) {
		sprintf(sTagId, "&newTagId=%d", newTagId);
		strcat(url, sTagId);
	}
	
	if (newDescription != "") {
    	sprintf(sDescription, "&newDescription=%s", convertPlainToURL(newDescription));
		strcat(url, sDescription);
	}
    
    if (newLock != null) {
    	if (newLock == true) {
    		strcat(url, "&newLock=true");
    	} else {
    		strcat(url, "&newLock=false");
    	}		
    }
	
    if (newProperties != "") {
		sprintf(body, "Body=%s", newProperties);
		web_reg_find("Text=\"status\":\"OK\"", LAST);
	    web_custom_request("UpdateBundleByTagId",
	        url, 
	        "Method=PUT", 
	        "EncType=application/json", 
	        body,
	        LAST); 
    } else {
    	web_reg_find("Text=\"status\":\"OK\"", LAST);
	    web_custom_request("UpdateBundleByTagId",
	        url, 
	        "Method=PUT", 
	        "EncType=application/json", 
	        LAST);
    }
}


/*************************************************************************
The set of methods to work on 'bundles' json object
**************************************************************************/

/*
Description:
	Returns number of bundles in the 'bundles' json object
Parameters:
	-
Example:

*/
int bundlesCount() {
	return lr_json_get_values("JsonObject=databucket", "ValueParam=bundleId", "QueryString=$..bundleId", "SelectAll=Yes", "NotFound=Continue", LAST);	
}

/*
Description:
	Creates 'bundle' json object based on 'bundles' json object
Parameters:
	id - the array element id
Example:

*/
void bundlesGetBundle(int id) {
	char* queryString = (char *) malloc(1 + strlen("QueryString=$.bundles[]") + 5);
	sprintf(queryString, "QueryString=$.bundles[%d]", id);	
	lr_json_get_values("JsonObject=databucket", "ValueParam=bundleParam", queryString, LAST);
    lr_eval_json("Buffer={bundleParam}", "JsonObject=bundle", LAST);
}


/*************************************************************************
The set of methods to work on 'bundle' json object
**************************************************************************/

/*
Description:
	Creates empty 'bundle' json object
*/
void bundleCreate() {
	lr_eval_json("Buffer=\{\"properties\":\{\}\}", "JsonObject=bundle", LAST);
}

/*
Description:
	Creates dynamic parameter 'bundleId' based on the 'bundle' json object
*/
void bundleGetBundleId() {
	lr_json_get_values("JsonObject=bundle", "ValueParam=bundleId", "QueryString=$.bundleId", LAST);
}


/*
Description:
	Creates dynamic parameter 'properties' based on 'bundle' json object
*/
void bundleGetProperties() {
	lr_json_get_values("JsonObject=bundle", "ValueParam=properties", "QueryString=$.properties", LAST);
	lr_convert_string_encoding(lr_eval_string("{properties}"), LR_ENC_SYSTEM_LOCALE, LR_ENC_UTF8, "properties");
}

/*
Description:
	Creates dynamic parameter with the specified name, based on 'bundle' json object
Parameters:
	name - the property name
*/
void bundleGetProperty(char* name) {
	int propCount = 0;
	char * valueParam = (char *) malloc(1 + strlen("ValueParam=") + strlen(name));
	char * queryString = (char *) malloc(1 + strlen("QueryString=$.properties.") + strlen(name));
		
	sprintf(valueParam, "ValueParam=%s", name);
	sprintf(queryString, "QueryString=$.properties.%s", name);
	
	propCount = lr_json_get_values("JsonObject=bundle", valueParam, queryString, "NotFound=Continue", LAST);
	if (propCount == 0) {
		lr_error_message("The property '%' was not found in the bundle.", name);
		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_AUTO);
	} 
}

/*
Description:
	Update existing property or add new one into the 'bundle' json object
Parameters:
	name - parameter name
	value - parameter value
*/
void bundleSetProperty(char* name, char* value) {
	const char queryStringConst[] = "QueryString=$.properties.";
	char * setQueryString = (char *) malloc(1 + strlen("QueryString=$.properties.") + strlen(name));
	sprintf(setQueryString, "QueryString=$.properties.%s", name);
	setValue(name, value, setQueryString, "QueryString=$.properties");
}

/*
Description:
	Pernamently removes the property from the 'bundle' json object
Parameters:
	name - parameter name
*/
void bundleDeleteProperty(char* name) {
	int i;
	
	char * queryString = (char *) malloc(1 + strlen("QueryString=$.properties.") + strlen(name));
	sprintf(queryString, "QueryString=$.properties.%s", name);
	
	i = lr_json_delete("JsonObject=bundle", queryString, "NotFound=Continue", LAST);
	
	if (i != 1)
		lr_error_message("lr_json_delete should return %d, but returns %d", 1, i);
}

/*
Description:
	Private method used by jsonBundle_SetProperty
Parameters:
	name - the parameter name
	value - the parameter value
	setQueryString - query string for lr_json_set_values method
	insertQueryString - query string for lr_json_insert	
*/
void setValue(char* name, char* value, char* setQueryString, char* insertQueryString) {
	int i;
	
	char * setValue = (char *) malloc(1 + strlen("Value=") + strlen("\"") + strlen(value) + strlen("\""));	
	char * insertValue = (char *) malloc(1 + strlen("Value=") + strlen("{\"") + strlen(name) + strlen("\":\"") + strlen(value) + strlen("\"}"));	
	sprintf(setValue, "Value=\"%s\"", value);
	
	lr_log_message("setValue: %s", setValue);
	lr_log_message("setQueryString: %s", setQueryString);
	
	// try to set value for property
	i = lr_json_set_values("JsonObject=bundle", setValue, setQueryString, "NotFound=Continue", LAST);	
	
	// if property doesn't exist, insert the property into properties array
	if (i < 1) {
		sprintf(insertValue, "Value={\"%s\":\"%s\"}", name, value);

		lr_log_message("insertValue: %s", insertValue);
		lr_log_message("insertQueryString: %s", insertQueryString);
		
		i = lr_json_insert("JsonObject=bundle", insertValue, insertQueryString, LAST);
		
		if (i != 1)
			lr_error_message("lr_json_insert should return %d, but returns %d", 1, i);
	}
}

/*
Description:
	Converts the given string to URL format
*/
char *convertPlainToURL(const char * plain) {
	lr_save_string(plain, "plain");
    web_convert_param("plain", "SourceEncoding=PLAIN","TargetEncoding=URL", LAST);
    return lr_eval_string("{plain}");
}
