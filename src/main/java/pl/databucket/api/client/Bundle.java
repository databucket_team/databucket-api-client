package pl.databucket.api.client;

import java.util.HashMap;

import com.google.gson.Gson;

/**
 * The class object represents one bundle (data row) from databucket database
 */
public class Bundle {

    private Integer tagId = null;
    private Integer bundleId = null;
    private String description = null;
    private Boolean locked = null;
    private HashMap<String, String> properties = null;   

    @Override
    public String toString() {
    	Gson gson = new Gson();
    	return gson.toJson(this);
    }
    
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public Integer getTagId() {
        return tagId;
    }

    public Integer getBundleId() {
        return bundleId;
    }

    public void setBundleId(Integer bundleId) {
        this.bundleId = bundleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setProperties(HashMap<String, String> properties) {
    	this.properties = properties;
    }
    
    public HashMap<String, String> getProperties() {
    	return properties;
    }
    
    /**
     * Gets the bundle property indicated by the specified key
     * @param key - the name of the property
     * @return the string value of the bundle property, or null if there is no property with that key
     */
    public String getProperty(String key) {
    	return properties.get(key);
    }
    
    /**
     * Gets the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     * @return the string value of the bundle property, or null if there is no property with that key
     */
    public String getProperty(Enum<?> key) {
    	return getProperty(key.name());
    }

    /**
     * Sets the bundle property indicated by the specified key
     * @param key - the name of the property
     * @param value - the value of the property
     */
    public void setProperty(String key, String value) {
    	if (properties == null)
    		properties = new HashMap<String, String>();
    	
    	properties.put(key, value);
    }
    
    /**
     * Sets the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     * @param value - the value of the property
     */
    public void setProperty(Enum<?> key, String value) {
    	setProperty(key.name(), value);
    }
    
    /**
     * Removes the bundle property indicated by the specified key
     * @param key - the name of the property
     */
    public void removeProperty(String key) {
    	properties.remove(key);
    }
    
    /**
     * Removes the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     */
    public void removeProperty(Enum<?> key) {
        removeProperty(key.name());
    }
    
    /**
     * Returns information about the bundle state (locked/unlocked)
     * @return true = locked, false - unlocked
     */
    public boolean isLocked() {
    	return locked;
    }
    
    /**
     * Sets the bundle state (locked/unlocked)
     * @param locked - new state: true - locked, false - unlocked
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
