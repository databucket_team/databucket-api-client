package pl.databucket.api.client;

import java.util.List;

public class ServiceResponse {

	private Status status = null;
	private String message = null;
	private Integer tablesCount = null;
	private List<Table> tables = null;
	private Integer tagId = null;
	private Integer tagsCount = null;
	private List<Tag> tags = null;
	private Integer bundlesCount = null;
	private List<Bundle> bundles = null;
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setTables(List<Table> tables) {
		this.tables = tables;
		this.tablesCount = tables.size();
	}
	
	public List<Table> getTables() {
		return tables;
	}
	
	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}
	
	public void setTags(List<Tag> tags) {
		this.tags = tags;
		this.tagsCount = tags.size();
	}
	
	public List<Tag> getTags() {
		return tags;
	}
	
	public void setBundles(List<Bundle> bundles) {
		this.bundles = bundles;
		this.bundlesCount = bundles.size();
	}
	
	public List<Bundle> getBundles() {
		return bundles;
	}

	public Integer getTablesCount() {
		return tablesCount;
	}

	public void setTablesCount(Integer tablesCount) {
		this.tablesCount = tablesCount;
	}
	
	public Integer getTagsCount() {
		return tagsCount;
	}

	public void setTagsCount(Integer tagsCount) {
		this.tagsCount = tagsCount;
	}

	public Integer getBundlesCount() {
		return bundlesCount;
	}

	public void setBundlesCount(Integer bundlesCount) {
		this.bundlesCount = bundlesCount;
	}

}
