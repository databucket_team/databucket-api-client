package pl.databucket.api.client;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class Databucket {
	
	private String databucketUrl;
	private Client client;
    private String userName = System.getProperty("user.name");
    private Gson gson;
	
    
    /**
     * Constructor.
     * @param databucketUrl - the URL for the Databucket REST Service.
     * @param enableLog - log in the System.out all requests and responses
     */
	public Databucket(String databucketUrl, boolean enableLog) {
		this.databucketUrl = databucketUrl;
		client = Client.create();
		if (enableLog)
			client.addFilter(new LoggingFilter(System.out));
		gson = new Gson();
	}
	
	/**
	 * Change default system user name to custom value
	 * @param userName - the user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Returns a map of the table tags.
	 * Examples:
	 * HashMap&lt;String, Integer&gt; tags = databucket.getTags(table);
	 * List&lt;Bundle&gt; bundles = databucket.getBundles(table, null, "" + tags.get("sampleTag"), null);
	 * @param tableName - the source table name
	 * @return List of pairs (tagName, tagId)
	 */
	public HashMap<String, Integer> getTags(String tableName) {
		String resource = String.format("/tables/%s/tags", tableName);
		        
        WebResource webResource = client.resource(databucketUrl + resource);
		webResource.accept(MediaType.APPLICATION_JSON);			
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
						
		if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (serviceResponse.getStatus().equals(Status.OK)) {
        		List<Tag> tags = serviceResponse.getTags();
        		HashMap<String, Integer> tagsMap = new HashMap<String, Integer>();
        		for (Tag tag : tags) {
        			tagsMap.put(tag.getTagName(), tag.getTagId());
        		}
        		
        		return tagsMap;
        	} else
        		new RuntimeException(serviceResponse.getMessage());
        } else 
			new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
					
		return null;
	}
	
	/**
	 * Inserts a set of properties into database
	 * @param tableName - the target table name
	 * @param tagId - the tag id for new bundle
	 * @param description - the bundle description
	 * @param lock - the bundle initial lock state
	 * @param properties - the bundle description
	 * @return created bundle
	 */
	public Bundle insertBundle(String tableName, int tagId, String description, boolean lock, HashMap<String, String> properties) {
		String method = String.format("/tables/%s/bundles", tableName);
		
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("userName", userName);
        queryParams.add("tagId", "" + tagId);
        queryParams.add("description", description);
        queryParams.add("lock", "" + lock);
        
		WebResource webResource = client.resource(databucketUrl + method);
		webResource.accept(MediaType.APPLICATION_JSON);
		
        ClientResponse response = webResource.queryParams(queryParams).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, gson.toJson(properties));
		
        if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (serviceResponse.getStatus().equals(Status.OK)) 
        		return serviceResponse.getBundles().get(0);
        	else
        		new RuntimeException(serviceResponse.getMessage());
        } else 
			new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
        
        return null;
	}
	
	/**
	 * Removes permanently the given bundles
	 * @param tableName - the source table name
	 * @param bundlesIds - the list of bundles ids
	 */
	public void deleteBundles(String tableName, String bundlesIds) {
		String method = String.format("/tables/%s/bundles", tableName);
				
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("bundlesIds", bundlesIds);
        
		WebResource webResource = client.resource(databucketUrl + method);
		webResource.accept(MediaType.APPLICATION_JSON);			
		ClientResponse response = webResource.queryParams(queryParams).delete(ClientResponse.class);
			
		if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (!serviceResponse.getStatus().equals(Status.OK)) 
        		new RuntimeException(serviceResponse.getMessage());
        } else 
			new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
	}	
	
	/**
	 * Returns the list of bundles for given filters
	 * @param tableName - the source table name
	 * @param bundlesIds - (filter) the list of bundles ids
	 * @param tagsIds - (filter) the list of tags ids
	 * @param properties - (filter) a string putting into sql select query - ...where properties like ('&lt;properties&gt;')
	 * @return the list of bundles
	 */
	public List<Bundle> getBundles(String tableName, String bundlesIds, String tagsIds, String properties) {
		String resource = String.format("/tables/%s/bundles", tableName);
			
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		if (bundlesIds != null)
			queryParams.add("bundlesIds", bundlesIds);
        
		if (tagsIds != null)
			queryParams.add("tagsIds", tagsIds);
        
		if (properties != null)
			queryParams.add("properties", properties);
        
        WebResource webResource = client.resource(databucketUrl + resource);
		webResource.accept(MediaType.APPLICATION_JSON);			
		ClientResponse response = webResource.queryParams(queryParams).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
						
		if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (serviceResponse.getStatus().equals(Status.OK)) 
        		return serviceResponse.getBundles();
        	else
        		new RuntimeException(serviceResponse.getMessage());
        } else 
			new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
					
		return null;			
	}

	/**
	 * Locks and returns the list of bundles for the given parameter
	 * @param tableName - the source table name
	 * @param tagsIds - the list of tags ids
	 * @param random - (true) get random bundle, (false) get first free bundle
	 * @param count - number of bundles
	 * @return bundles
	 */
	public List<Bundle> lockBundles(String tableName, String tagsIds, boolean random, int count) {
		
		String resource = String.format("/tables/%s/bundles/lock", tableName);
			
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("userName", userName);
        queryParams.add("tagsIds", tagsIds);
        queryParams.add("random", "" + random);
        queryParams.add("count", "" + count);
        
		WebResource webResource = client.resource(databucketUrl + resource);
		webResource.accept(MediaType.APPLICATION_JSON);			
		ClientResponse response = webResource.queryParams(queryParams).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
						
		if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (serviceResponse.getStatus().equals(Status.OK)) 
        		return serviceResponse.getBundles();
        	else
        		new RuntimeException(serviceResponse.getMessage());
        } else 
			new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
					
		return null;
	}

	/**
	 * Updates the bundle
	 * @param tableName - the source table name
	 * @param bundleId - the bundle id
	 * @param newTagId - (optional) new tag id
	 * @param newDescription - (optional) new description
	 * @param newLock - (optional) lock (true) or unlock (false)
	 * @param newProperties - (optional) new properties
	 */
	public void updateBundle(String tableName, int bundleId, Integer newTagId, String newDescription, Boolean newLock, HashMap<String, String> newProperties) {
		String resource = String.format("/tables/%s/bundles", tableName);
		
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("userName", userName);
		queryParams.add("bundleId", "" + bundleId);
		
		// Optional
		if (newTagId != null)
			queryParams.add("newTagId", "" + newTagId);
		
		if (newDescription != null)
			queryParams.add("newDescription", newDescription);
		
		if (newLock != null)
			queryParams.add("newLock", "" + newLock);
		
		WebResource webResource = client.resource(databucketUrl + resource);
		webResource.accept(MediaType.APPLICATION_JSON);
		
		ClientResponse response = null;
		if (newProperties != null)
			response = webResource.queryParams(queryParams).type(MediaType.APPLICATION_JSON).put(ClientResponse.class, gson.toJson(newProperties));
		else
			response = webResource.queryParams(queryParams).type(MediaType.APPLICATION_JSON).put(ClientResponse.class);
				
		if (response.getStatus() == 200) {
        	ServiceResponse serviceResponse = gson.fromJson(response.getEntity(String.class), ServiceResponse.class);
        	if (!serviceResponse.getStatus().equals(Status.OK)) 
        		throw new RuntimeException(serviceResponse.getMessage());
        } else 
        	throw new RuntimeException("Failed : HTTP error code : " + response.getStatus() + response.getEntity(String.class));
	}	
}
